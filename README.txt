Getting started with Blends Web Sentinels
=========================================

- Create a local UDD instance on your local machine by following the instructions mentioned in:
   https://wiki.debian.org/UltimateDebianDatabase/Hacking

- Clone the Blends Web Sentinels Git repository:
	$ sudo mkdir -p /srv
	$ sudo chmod <enable write permission to you on /srv>
	$ cd /srv
	$ git clone https://salsa.debian.org/blends-team/website.git blends.debian.org

- Install all the dependencies mentioned in './blends.debian.org/webtools/0dependencies'

- Run the scripts using the following format: 
	$ <script> <blend>
	eg:  $ ./tasks.py debian-junior


To learn more about Blends, refer to the Blends documentation:
	http://blends.debian.org/blends/
