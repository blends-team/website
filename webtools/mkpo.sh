#!/bin/sh -e
NAME=blends-webtools
TMPPOT=tmp.pot

cd po
locales=$(ls -1 ??.po | sed 's?\(..\)\.po?\1?')

cat ${NAME}.pot > "$TMPPOT"
cat php-message-strings.pot >> "$TMPPOT"
cat static.pot >> "$TMPPOT"

for lang in $locales
do
	msgmerge -iU --width=100 "$lang".po "$TMPPOT"
done

rm -rf "$TMPPOT"

exit 0
