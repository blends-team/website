#!/usr/bin/python3
# create set of subscribe statements for ddtp for those packages
# that are listed in the Dependencies in the tasks files of a project
# use ddpo_register to finally send the mail

from sys import argv, exit, stderr

from blendstasktools import Tasks

if len(argv) <= 1:
    stderr.write("Usage: %s <Blend name>\n" +
                 "       The <Blend name> needs a matching config file webconf/<Blend name>.conf\n"
                 % argv[0])
    exit(-1)

tasks  = Tasks(argv[1])
if tasks.data['pkglist'] == '':
    stderr.write("Config file webconf/%s.conf is lacking pkglist field.\n" % argv[1])
    exit(-1)

tasks.GetAllDependencies(source=1)
packages = tasks.GetNamesOnlyDict(dependencystatus=['official_high', 'official_low',
                                                    'non-free', 'experimental'])

print("user %s" % tasks.data['pkglist'])
for task in list(packages.keys()):
    for pkg in packages[task]:
        print("subscribe %s %s" % (pkg, task))
print("thanks")


# Perhaps we should also send a mail to pts@qa.debian.org
#     keyword %(pkg) %(list) = bts bts-control upload-source katie-other summary default cvs ddtp derivatives contact
# to make sure the mailing list gets full information about packages including new upstream etc
# see http://www.debian.org/doc/manuals/developers-reference/resources.html#pkg-tracking-system
