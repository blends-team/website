#!/usr/bin/python3
# This CGI script should receive calls from a Salsa Web hook
# The web hook is fired if some change in the metapackage data
# of some Blend was commited and is triggering the recreation
# of the according tasks pages.

import cgi, cgitb
import os
import re
import subprocess
import sys

#Create instance of FieldStorage
form = cgi.FieldStorage() 
cgitb.enable()

BLENDSDIR="/srv/blends.debian.org/webtools"

def system_call(command):
    p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
    return p.stdout.read()

# Get valid Blend names
command = ''
strconf = str(system_call("ls %s/webconf/*.conf | grep -v -e 'webconf/fun\\.' -e rest-test | sed 's?^.*webconf/\\(.*\\)\\.conf?\\1?'" % BLENDSDIR))
strconf = re.sub("\s*'\s*$", "", re.sub("^b'\s*", '', strconf))
valid_blends = []
for v in strconf.split('\\n'):
    v = v.strip()
    if len(v) > 0:
        valid_blends.append(v)

# Parse arguments and call script if arguments are valid
output=''
for key in form.keys():
    variable = str(key)
    value = str(form.getvalue(variable))
    if variable == "blend":
        if value in valid_blends:
            command = "/srv/blends.debian.org/webtools/tasks.py " + str(value)
            system_call(command)
            output = "Calling: " + command
        else:
            output = output + "<br />Error: value given for parameter blend %s is no valid blend.  Thus do nothing." % value
    else:
        output = output + "<br />Invalid parameter %s for script." % variable

# Create fake output
HEAD="""Content-Type: text/html

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/2001/REC-xhtml11-20010531/DTD/xhtml11-flat.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
<head>
<title>webhook</title>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<meta name="author" content="Andreas Tille" />
</head>
<body>
Not a Gitlab Push Hook event<br />
%s
</body>
""" % (output)

print(HEAD)
