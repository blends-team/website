#!/bin/sh
sed \
    -e 's%\(<title>$projectname\)\( Thermometer</title>\)%\1 packages in Ubuntu\2%' \
    -e 's%\(<caption><h3>$projectname \)Package\( Thermometer</h3></caption>\)%\1 packages in Ubuntu\2%' \
    -e 's%\(<th>stable</th><th>testing</th><th>unstable</th><th>stable-bpo</th>\)<th>experimental</th><th>UNRELEASED</th>%\1<th>precise</th><th>quantal</th><th>raring</th>%' \
    -e 's%''${pkg.debianstatus}''%''${pkg.ubuntustatus}''%' \
    -e 's%\(<td>${pkg.stable}</td><td>${pkg.testing}</td><td>${pkg.unstable}</td><td>${pkg.stable_bpo}</td>\)<td>${pkg.experimental}</td><td>${pkg.UNRELEASED}</td>%\1<td>${pkg.precise}</td><td>${pkg.quantal}</td><td>${pkg.raring}</td>%' \
    -e 's%\(<div class="link"><a href=".\)u\(thermometer">\)Ubuntu\( Thermometer</a></div>\)%\1\2Debian\3%' \
    thermometer.xhtml > uthermometer.xhtml
