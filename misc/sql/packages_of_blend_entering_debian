#!/bin/sh
# get the list of source packages maintained by a Blends team
# The list is ordered by the date the source has entered the first
# time (when it was uploaded by the team) the newest package on top

if [ $# -lt 1 ] ; then
    echo "Usage: $0 <blend>"
    exit 1
fi


case "$1" in
    debian-science)
        team="'debian-science-maintainers@lists.alioth.debian.org','pkg-scicomp-devel@lists.alioth.debian.org','pkg-electronics-devel@lists.alioth.debian.org'"
        ;;
    debian-med)
        team="'debian-med-packaging@lists.alioth.debian.org'"
        ;;
    debian-gis)
        team="'pkg-grass-devel@lists.alioth.debian.org','pkg-osm-maint@lists.alioth.debian.org'"
        ;;
    debian-astro)
        team="'debian-astro-maintainers@lists.alioth.debian.org'"
        ;;
    debichem)
        team="'debichem-devel@lists.alioth.debian.org'"
        ;;
    *)
        echo "Unsupported Blend $1"
        exit 1
        ;;
esac

if [ ! $(which psql) ] ; then
    cat <<EOT
No PostgreSQL client providing /usr/bin/psql installed.
On a Debian system please
    sudo apt-get install postgresql-client-common
EOT
    exit 1
fi

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql $PORT -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

# Check UDD connection
if ! psql $PORT $SERVICE -c "" 2>/dev/null ; then
    echo "No local UDD found, use public mirror."
    PORT="--port=5432"
    export PGPASSWORD="public-udd-mirror"
    SERVICE="--host=public-udd-mirror.xvm.mit.edu --username=public-udd-mirror udd"
fi

psql $SERVICE >$1.out <<EOT
  SELECT source, date FROM
     (SELECT source, date, row_number() OVER (PARTITION BY source ORDER BY date ASC) FROM upload_history WHERE maintainer_email IN ($team) ) tmp
      WHERE row_number = 1 ORDER BY date DESC;
  ;
EOT

exit 0

