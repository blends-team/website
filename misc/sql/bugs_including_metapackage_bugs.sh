#!/bin/sh
# query fo rbugs
# Used to rewrite bugs pages for web sentinel based on UDD query only

if [ $# -lt 1 ] ; then
    echo "Usage: $0 <blendname>"
    exit
fi

psql udd > $1_buggy_packages.out <<EOT
SELECT distinct sources.source, tasks.tasks, CASE WHEN dependency = 'd' AND component = 'main' THEN 'depends' ELSE 'suggests' END AS status, version, homepage, vcs_browser, maintainer
  FROM (
  SELECT s.source, b.dependency, b.component, s.homepage, s.vcs_browser, s.maintainer, s.version, row_number() OVER (PARTITION BY s.source ORDER BY s.version DESC)
    FROM blends_dependencies b
    JOIN packages p ON p.package = b.package
    JOIN bugs bu    ON bu.source = p.source
    JOIN sources s  ON s.source  = p.source
    WHERE (blend = '$1' AND b.distribution = 'debian')
       OR s.source IN (SELECT DISTINCT p.source FROM blends_tasks bt
                       JOIN packages p ON p.package = bt.metapackage_name
                       WHERE blend = 'debian-med' and metapackage)
  GROUP BY s.source, b.dependency, b.component, s.homepage, s.vcs_browser, s.maintainer, s.version
  ) sources
  LEFT OUTER JOIN (
    SELECT source, array_agg(task ORDER BY task) AS tasks FROM (
     SELECT DISTINCT p.source, b.task
      FROM packages p
      JOIN releases r ON p.release = r.release
      JOIN blends_dependencies b ON b.package = p.package
      JOIN sources s ON p.source = s.source AND p.release = s.release
      WHERE b.blend = '$1'
     UNION
     SELECT DISTINCT p.source, bt.task FROM blends_tasks bt
      JOIN packages p ON p.package = bt.metapackage_name
      WHERE blend = '$1' and metapackage
    ) tmp
    GROUP BY source
  ) tasks ON sources.source = tasks.source
  WHERE row_number = 1
  ORDER BY source;
EOT

psql udd > $1_bugs_packages.out <<EOT
SELECT source, bu.id, title, status, done_by, tags FROM (
  SELECT distinct bu.source, bu.id, bu.title, bu.status, bu.done AS done_by
    FROM blends_dependencies b
    JOIN packages p ON p.package = b.package
    JOIN bugs bu    ON bu.source = p.source
    WHERE (blend = '$1' AND b.distribution = 'debian') OR  -- ... inside the metapackage source itself
       bu.source IN (SELECT DISTINCT p.source FROM blends_tasks bt
                       JOIN packages p ON p.package = bt.metapackage_name
                       WHERE blend = 'debian-med' and metapackage)
  ) bu
  LEFT OUTER JOIN (
    SELECT id, array_agg(tag ORDER BY tag) AS tags FROM bugs_tags GROUP BY id
  ) bt ON bu.id = bt.id
  ORDER BY source, bu.id;
EOT

