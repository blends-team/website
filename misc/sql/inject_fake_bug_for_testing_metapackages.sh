#!/bin/sh

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

psql $SERVICE >$1.out <<EOT
  INSERT INTO bugs (id, package, source, arrival, status, severity, submitter, title)
         VALUES(1, 'med-tools', 'debian-med', '2013-07-18 00:00:00', 'pending', 'important', 'Andreas Tille <tille@debian.org>', 'This is only a test for Blends tools - no real bug') ;
EOT
