// SPDX-License-Identifier: AGPL-3.0-or-later
/*
  @licstart  The following is the entire license notice for the
  JavaScript code in this page.

  Copyright (C) 2020  Debian Pure Blends Team

  The JavaScript code in this page is free software: you can
  redistribute it and/or modify it under the terms of the GNU
  General Public License (GNU GPL) as published by the Free Software
  Foundation, either version 3 of the License, or (at your option)
  any later version.  The code is distributed WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

  As additional permission under GNU GPL version 3 section 7, you
  may distribute non-source (e.g., minimized or compacted) forms of
  that code without the copy of the GNU GPL normally required by
  section 4, provided you include this license notice and a URL
  through which recipients can access the Corresponding Source.

  @licend  The above is the entire license notice
  for the JavaScript code in this page.
*/

/*
 * Dropdowns.
 */
window.addEventListener('DOMContentLoaded', () => {
    const dropdowns = document.querySelectorAll('.dropdown');
    for (const element of dropdowns) {
        element.classList.add('with-js');
        element.querySelector('p').addEventListener('click', () => {
            element.classList.toggle('open');
        });
    }
});

window.addEventListener('click', (event) => {
    const dropdowns = document.querySelectorAll('.dropdown');
    for (const element of dropdowns) {
        if (!element.contains(event.target) &&
            element.classList.contains('open')) {
            element.classList.remove('open');
        }
    }
});

/*
 * Packages in QA page.
 */
window.addEventListener('DOMContentLoaded', () => {
    document.querySelector('.packages').classList.add('with-js');
    const elements = document.querySelectorAll('.package-header');
    for (const element of elements) {
        element.addEventListener('click', () => {
            element.closest('.package').classList.toggle('show-full');
        });
    }
});
